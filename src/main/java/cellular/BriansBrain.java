package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {

    IGrid currentGeneration;

    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public CellState getCellState(int row, int column) {
        return currentGeneration.get(row, column);
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                currentGeneration.set(row, col, CellState.random(random));
            }
        }
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();

        for (int row=0; row<numberOfRows(); row++) {
            for (int col=0; col<numberOfColumns(); col++) {
                nextGeneration.set(row, col, getNextCell(row, col));
            }
        }

        currentGeneration = nextGeneration;
    }

    @Override
    public CellState getNextCell(int row, int col) {
        CellState currentCell = getCellState(row, col);
        int livingNeighbours = countNeighbors(row, col, CellState.ALIVE);

        if (currentCell.equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        else if (currentCell.equals(CellState.DEAD) && livingNeighbours == 2) {
            return CellState.ALIVE;
        }
        return CellState.DEAD;
    }

    private int countNeighbors(int row, int col, CellState state) {
        int stateCounter = 0;
        for (int i=row-1; i<=row+1; i++) {
            for (int j=col-1; j<=col+1; j++) {
                if (i>=0 && j>=0 && i<numberOfRows() && j<numberOfColumns()) {
                    if (currentGeneration.get(i,j).equals(state)) {
                        stateCounter++;
                    }
                }
            }
        }
        if (currentGeneration.get(row, col).equals(state)) {
            stateCounter--;
        }
        return stateCounter;
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}

package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int columns;
    CellState[][] cellStates;

    public CellGrid(int rows, int columns, CellState initialState) {
        this.rows = rows;
        this.columns = columns;
        cellStates = new CellState[rows][columns];
        for (int i=0; i<rows; i++) {
            for (int j=0; j<columns; j++) {
                cellStates[i][j] = initialState;
            }
        }
    }

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return columns;
    }

    @Override
    public void set(int row, int column, CellState element) {
        cellStates[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return cellStates[row][column];
    }

    @Override
    public IGrid copy() {
        IGrid grid = new CellGrid(rows, columns, CellState.DEAD);
        for (int i=0; i<rows; i++) {
            for (int j=0; j<columns; j++) {
                grid.set(i, j, cellStates[i][j]);
            }
        }
        return grid;
    }
    
}
